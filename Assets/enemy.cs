﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    public float vida;
    public GameObject sangre;
    
    
    public void Restador(float restador)
    {

        vida = vida - restador;
        if (vida <= 0)
        {
            
            Die();
        }

    }


    public void SpawnFx(Vector2 bulletPos)
    {


        GameObject fx = Instantiate(sangre, transform.position, Quaternion.Euler(0, 0, 90));

        if (bulletPos.x>transform.position.x)
        {
            fx.GetComponent<SpriteRenderer>().flipY = true;
        }


        //sangre.GetComponent<Renderer>().enabled = false;



        //la destruccion se hace desde un evento de animación
        //Destroy(fx, lifetime);
    }

    /*  void OnCollisionEnter2D(Collision2D collision)
      {
          //diferencia con script bulletcontroller linea *12, aqui usamos gameobject desues de collision. por ONTRiggeredEnter2D vs OnCollisionEnter2D

          if (collision.gameObject.CompareTag("balas"))



      }*/

    void Die()
    { //die effects
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().EnemyDie();
        FindObjectOfType<spawn>().SpawnZombi();
        Destroy(gameObject);

    }
    // Start is called before the first frame update
    void Start()
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().AddEnemy();
    }


    // Update is called once per frame
    void Update()
    {
       
    }
}
