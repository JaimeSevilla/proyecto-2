﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieIA : MonoBehaviour
{
    public float velocity;
    Transform player;
    Rigidbody2D RB;
    public enum estados {chasing, waiting};
    public estados miEstado = estados.waiting;
    float timeToFlip = 2;
    bool right;
    public float distanciaDetection;
    public LayerMask enemigoLayer;
    SpriteRenderer spr;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>().transform;
        RB = GetComponent<Rigidbody2D>();
        spr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (player.position.y>transform.position.y+2.5 || player.position.y < transform.position.y - 2.5)
        {
            miEstado = estados.waiting;
        }
        else
        {
            if (Vector2.Distance(player.position, transform.position) < distanciaDetection) 
            {
                miEstado = estados.chasing;
                AvisarZombis();
            }

           
        }
    }
   /* public void melee()
    {
        if (player.position.x)
        {

        }
    }
   */

    void AvisarZombis()
    {
        Collider2D[] enemigos = Physics2D.OverlapCircleAll(transform.position, 5, enemigoLayer);

        /* for (int i = 0;i< enemigos.Length ;i++ )

         {
             enemigos[i].GetComponent<zombieIA>().SetChasing();

         }
        */

        foreach (Collider2D item in enemigos)
        {
            item.GetComponent<zombieIA>().SetChasing();
        }

    }

    public void SetChasing()
    {
        miEstado = estados.chasing;


    }
  
    //para la gestión de físicas
    void FixedUpdate()
    {

        switch (miEstado)
        {
            case estados.chasing:

                anim.SetBool("running", true);
                if (player.position.x > transform.position.x)

                {
                    
                    right = true;
                    RB.velocity = new Vector2(velocity * Time.deltaTime, RB.velocity.y);
                    spr.flipX = false;


                }

                if (player.position.x < transform.position.x)
                { 
                    right = false;
                    RB.velocity = new Vector2(-velocity * Time.deltaTime, RB.velocity.y);
                   spr.flipX = true;
                }

                break;

            case estados.waiting:
            anim.SetBool("running", true);
                if (timeToFlip<=0)
                {
                    timeToFlip = 2;
                    if (right==true)
                    {
                        
                        right = false;
                        spr.flipX = true;
                    }
                    else
                    {  
                        right = true;
                        spr.flipX = false;
                    }
                }
                else
                {//timeToFlip= timeToflip-time.deltatime
                    timeToFlip -= Time.deltaTime;
                }
                if (right==true)

                {
                    
                    RB.velocity = new Vector2((velocity/2) * Time.deltaTime, RB.velocity.y);
                }
                else
                {
                    
                    RB.velocity = new Vector2(-(velocity/2) * Time.deltaTime, RB.velocity.y);

                }
                break;
            default:
                break;
        }


        
    }
}
