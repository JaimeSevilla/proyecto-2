﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayeronCollision : MonoBehaviour
{
    public Vector2 hitforce;
    public float damage;

    void OnTriggerEnter2D(Collider2D collision)
    {
        //diferencia con script bulletcontroller linea *12, aqui usamos gameobject desues de collision. por ONTRiggeredEnter2D vs OnCollisionEnter2D

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().Restador(damage, hitforce, transform);
        }


    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        //diferencia con script bulletcontroller linea *12, aqui usamos gameobject desues de collision. por ONTRiggeredEnter2D vs OnCollisionEnter2D

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().Restador(damage, hitforce, transform);
        }


    }
    void OnCollisionStay2D(Collision2D collision)
    {


        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().Restador(damage, hitforce, transform);
        }


    }
    void OnTriggerStay2D(Collider2D collision)
    {
        

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<PlayerController>().Restador(damage, hitforce, transform);
        }


    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
