﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickUp : MonoBehaviour
{
    public PlayerController.armas arma;
    public float fireRate;
    public int ammo;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>() != null)
        {
            collision.GetComponent<PlayerController>().pickedUpGun(arma, ammo, fireRate);

            Destroy(gameObject);
        }
        

    }
    // Start is called before the first frame update
    void Start()
    {
       

    }

    // Update is called once per frame
    void Update()
    {
        if (ammo <= 0)
        {
            fireRate = 0.2f;
        }


    }
    
}
