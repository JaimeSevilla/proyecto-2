﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;


public class BulletController : MonoBehaviour
{
    
    float speed;
    Rigidbody2D RB;
    public float damage;
    public float lifetime;
    public PlayerController.armas tipo;
    public LayerMask enemigoLayer;
    public GameObject explosionFx;
    public GameObject explosionRangoFX;
    public float radius;



private void OnTriggerEnter2D(Collider2D collision)
    {
        enemy colliderEnemy =collision.GetComponent<enemy>();
        if (colliderEnemy!=null)
        {
            if (tipo==PlayerController.armas.RPG)
            {
                Explotar();

                return;
            }
           
            colliderEnemy.Restador(damage);
            colliderEnemy.SpawnFx(transform.position);
            //cambiar el
            //collision.GetComponent<activarFX>().Inicializar(Mathf.Abs(colliderEnemy.gameObject.transform.localScale.x));
            //colliderEnemy = null;
            Destroy(gameObject);
        }
        if (collision.CompareTag("Ground"))
        {
            if (tipo == PlayerController.armas.RPG)
            {
                Explotar();

                return;
            }

            Destroy(gameObject);
        }
        

    }
    void Explotar()
    {
        //Instanciar efecto explosión
        Collider2D[] enemigos = Physics2D.OverlapCircleAll(transform.position, radius, enemigoLayer);

        for (int i = 0; i < enemigos.Length; i++)
        {
            enemigos[i].GetComponent<enemy>().Restador(damage);
        }

        Instantiate(explosionFx, transform.position, Quaternion.identity);
        GameObject instanciaFX = Instantiate(explosionRangoFX, transform.position, Quaternion.identity);
        instanciaFX.transform.localScale *= (radius * 2);
        Destroy(gameObject);
    }
    public void SetSpeed(float speed)
    {
      this.speed = speed;
    }
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifetime);
        RB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RB.velocity = Vector2.right * speed * Time.deltaTime;

       
        
    }
}
