﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public string Scene;
    public void Play()
    {
        SceneManager.LoadScene(Scene);

    }

    public void Exit()
    {
        Application.Quit();

    }
    // Start is called before the first frame update

    
}
