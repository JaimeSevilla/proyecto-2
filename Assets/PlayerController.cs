﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[SelectionBase]
public class PlayerController : MonoBehaviour
{
    Collider2D mycollider;
    public Vector2 jumpHeight;
    public float speed;
    Rigidbody2D RB;
    public float jumpForce;
    public Transform detector;
    public LayerMask plataforma;
    public bool right = true;
    public float bulletSpeed;
    bool canShoot = true;
    public GameObject gunI;
    public GameObject gunD;
    float vida;
    public float maxVida;
    bool inmune;
    public float tinmune;
    public enum estados { free, stunned }
    public estados miEstado;
    public SpriteRenderer spr;
    public float stunned;
    public enum armas { RPG, PKM, RayGun, Basic }
    public armas currentArma;
    [Header("Disparo")]
    int ammo;
    public float FireRate;
    public float FireRateRPG;
    public GameObject balaRPG;
    public GameObject balaBasic;
    GameObject currentBullet;
    private Animator anim;
    int enemyCount = 0;
    public Text contadorEnemigos;
    public Image iconoVida;
    public Image iconoRpg;
    public Image iconoPistola;
    public Text contadorBalasRpg;
    void FixedUpdate()
    {


        switch (miEstado)
        {
            case estados.free:
                Move();

                break;
            case estados.stunned:
                break;
            default:
                break;
        }

    }
    public void pickedUpGun(armas tipo, int ammo, float fireRate)
    {
        currentArma = tipo;
        this.ammo = ammo;
        this.FireRate = fireRate;
        this.FireRateRPG = fireRate;
        
        switch (tipo)
        {
            case armas.RPG:
                currentBullet = balaRPG;
                iconoPistola.enabled = false;
                iconoRpg.enabled = true;
                contadorBalasRpg.enabled = true;
                contadorBalasRpg.text += ammo.ToString();
                break;
            case armas.PKM:
                break;
            case armas.RayGun:
                break;
            case armas.Basic:

                currentBullet = balaBasic;
                iconoRpg.enabled = false;
                break;
            default:
                
                break;
        }


    }
    public void Restador(float restador, Vector2 hitforce, Transform position)
    {
        if (inmune == false)
        {

            vida = vida - restador;
            KnockBack(hitforce, position);
            StartCoroutine(InmuneTime());
            StartCoroutine(FlashDamage());
            StartCoroutine(Stunned());



            if (vida <= 0)
            {
                Die();
            }
            if(vida <= 100)
            {
                iconoVida.enabled = false;
            }

        }
    }

    IEnumerator Stunned()
    {
        miEstado = estados.stunned;
        yield return new WaitForSeconds(stunned);
        miEstado = estados.free;
    }
    public void KnockBack(Vector2 hitforce, Transform position)
    {
        RB.velocity = Vector2.zero;
        if (position.position.x < transform.position.x)
        {

            RB.AddForce(hitforce);
        }
        else
        {
            RB.AddForce(new Vector2(-hitforce.x, hitforce.y));
        }

    }
    IEnumerator FlashDamage()
    {
        Color oldColor = spr.color;

        spr.color = Color.red;

        yield return new WaitForSeconds(0.2f);

        spr.color = oldColor;


    }
    IEnumerator InmuneTime()
    {
        inmune = true;

        yield return new WaitForSeconds(tinmune);
        inmune = false;
    }
    void Die()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);


        Debug.Log("muerto");

    }
    public void EnemyDie()
    {
        //Llamamos desde enemigo a esta funcion para decir al jugador que el enemigo esta muerto
        enemyCount--;
        contadorEnemigos.text = "Enemigos: " + enemyCount;

    }
    public void AddEnemy()
    {
        enemyCount++;
        contadorEnemigos.text = "Enemigos: " + enemyCount;
    }
    void Move()
    {
        if (Input.GetAxisRaw("Horizontal") == 1)
        {
            RB.velocity = new Vector2(speed * Time.fixedDeltaTime, RB.velocity.y);
            right = true;

            spr.flipX = false;
            gunD.SetActive(true);
            gunI.SetActive(false);
            anim.SetBool("running", true);
        }
        if (Input.GetAxisRaw("Horizontal") == -1)
        {
            RB.velocity = new Vector2(-speed * Time.fixedDeltaTime, RB.velocity.y);
            right = false;

            spr.flipX = true;
            gunD.SetActive(false);
            gunI.SetActive(true);
            anim.SetBool("running", true);
        }
        if (Input.GetAxisRaw("Horizontal") == 0)
        {
            anim.SetBool("running", false);
        }
    }


    void Jump(bool up)
    {
        Collider2D currentPlatform = Physics2D.OverlapCircle(detector.position, 0.2f, plataforma);
        if (up && currentPlatform != null)
        {
            RB.velocity = new Vector2(RB.velocity.x, 0);
            RB.AddForce(Vector2.up * jumpForce);
        }

        else if (!up && currentPlatform.GetComponent<PlatformEffector2D>() != null)
        {
            StartCoroutine(GetDown(currentPlatform));
        }
    }




    IEnumerator Shoot()
    {
        // quaternion.identity indica que no tiene rotación.

        if (canShoot == true)
        {
            if (ammo > 0 || currentArma == armas.Basic)
            {


                if (currentArma == armas.RPG)
                {
                    ammo--;
                    currentBullet = balaRPG;
                    contadorBalasRpg.text ="Balas: "+ ammo.ToString();

                    if (ammo == 0)
                    {
                        iconoRpg.enabled = false;
                        contadorBalasRpg.enabled = false;
                        iconoPistola.enabled = true;
                        currentArma = armas.Basic;
                        FireRate = 0.2f;
                        currentBullet = balaBasic;
                        ammo = 1;
                    }

                }



                canShoot = false;


                GameObject bala = Instantiate(currentBullet, transform.position, Quaternion.identity);


                if (right)
                {
                    bala.GetComponent<BulletController>().SetSpeed(bulletSpeed);


                    bala.transform.position = gunD.transform.GetChild(0).transform.position;
                }
                else
                {
                    bala.GetComponent<BulletController>().SetSpeed(-bulletSpeed);
                    bala.transform.position = gunI.transform.GetChild(0).transform.position;
                }

                yield return new WaitForSeconds(FireRate);
                canShoot = true;
            }
        }
    }
    IEnumerator GetDown(Collider2D Platform)
    {
        Physics2D.IgnoreCollision(Platform, GetComponent<Collider2D>(), true);
        yield return new WaitForSeconds(0.7f);
        Physics2D.IgnoreCollision(Platform, GetComponent<Collider2D>(), false);

    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(detector.position, 0.2f);
    }
    // Start is called before the first frame update

    void Start()
    {
        vida = maxVida;
        RB = GetComponent<Rigidbody2D>();
        currentBullet = balaBasic;
        anim = GetComponentInChildren<Animator>();
        mycollider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()

    {
        //transform.Translate(playerSpeed * Time.deltaTime, 0f, 0f);  //makes player run

        //makes player jump
        {

            switch (miEstado)
            {
                case estados.free:

                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        Jump(true);

                    }
                    else if (Input.GetKeyDown(KeyCode.S))
                    {
                        Jump(false);

                    }

                    if (Input.GetButtonDown("Fire1"))
                    {
                        StartCoroutine(Shoot());

                    }
                    break;
                case estados.stunned:
                    break;
                default:
                    break;
            }

        }
    }
}

